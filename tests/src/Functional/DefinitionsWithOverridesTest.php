<?php

namespace Drupal\Tests\queue_order\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class DefinitionsWithModuleTest.
 *
 * @package Drupal\queue_order\Tests\Kernel
 *
 * @group queue_order
 */
class DefinitionsWithOverridesTest extends BrowserTestBase {

  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'queue_order_overrides_fixtures',
    'queue_order',
  ];

  /**
   * Queue Worker Manager service.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $QueueWorkerManager;

  protected $orderedList = [
    'queue_order_worker_F',
    'queue_order_worker_E',
    'queue_order_worker_D',
    'queue_order_worker_C',
    'queue_order_worker_B',
    'queue_order_worker_A',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->QueueWorkerManager = $this->container->get('plugin.manager.queue_worker');
  }

  /**
   * Test equality of Queue Worker definition order.
   */
  public function testOrder() {
    $this->assertEquals(
      $this->orderedList,
      array_keys($this->QueueWorkerManager->getDefinitions()),
      'Order is not managed by the config settings'
    );
    $this->assertSame(
      $this->orderedList,
      array_keys($this->QueueWorkerManager->getDefinitions()),
      'Order is not managed by the config settings'
    );
  }

}
