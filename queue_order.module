<?php

/**
 * @file
 * Contains queue_order.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function queue_order_help($route_name, RouteMatchInterface $route_match) {
  // Todo: Remove hook implementation when Help Topics become stable.
  switch ($route_name) {
    case 'help.page.queue_order':
      $output = '';
      $output .= '<h3>' . t('About Queue Order module') . '</h3>';
      $output .= '<p>' . t('This is the module that provide functionality of sorting queue workers definitions. That causes an effect on queue execution order during cron run.') . '</p>';
      $output .= '<h3>' . t('Configuration') . '</h3>';
      $output .= '<p>' . t('Weight value of queue worker can be defined in the "weight" property of the queue worker definition.') . '</p>';
      $output .= '<p>' . t('All defined queue workers have default weight value that equal to "0".') . '</p>';
      $output .= '<p>' . t('To override default queue worker weight property value use "order" property of "queue_order.settings" config object.') . '</p>';
      $output .= '<p>' . t('All overridden weight values of queue workers stored in "order" property of "queue_order.settings" config object.') . '</p>';
      $output .= '<p>' . t('It contains key - value array, where key is the id of queue worker, value - the weight value.') . '</p>';
      $output .= '<p>' . t(
          'To manage queue worker definitions order overrides use the <a href=":ui_project_page">Queue UI module</a>.',
          [':ui_project_page' => 'https://www.drupal.org/project/queue_ui']
        ) . '</p>';
      $output .= '<p>' . t('Kindly refer the README.md file for more details.') . '</p>';
      return $output;
  }
}
